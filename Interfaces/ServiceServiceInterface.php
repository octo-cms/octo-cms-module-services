<?php

namespace OctoCmsModule\Services\Interfaces;

use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Services\Entities\Service;

/**
 * Interface ServicesServiceInterface
 *
 * @package OctoCmsModule\Services\Interfaces
 */
interface ServiceServiceInterface
{
    /**
     * @param Service $service
     * @param array   $fields
     *
     * @return Service
     */
    public function saveService(Service $service, array $fields): Service;

    /**
     * @param Service $service
     *
     * @return Page
     */
    public function storeServicePage(Service $service): Page;
}
