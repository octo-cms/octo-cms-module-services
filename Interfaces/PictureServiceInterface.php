<?php

namespace OctoCmsModule\Services\Interfaces;

/**
 * Interface PictureServiceInterface
 *
 * @package OctoCmsModule\Services\Interfaces
 */
interface PictureServiceInterface extends \OctoCmsModule\Core\Interfaces\PictureServiceInterface
{

}
