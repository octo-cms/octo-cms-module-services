const routes = {
    DATATABLES: {
        services: 'api/admin/v1/datatables/services',
    },

    SERVICE_STORE: 'api/admin/v1/services',
    SERVICE_GET: 'api/admin/v1/services/{id}',
    SERVICE_UPDATE: 'api/admin/v1/services/{id}',
    SERVICE_DELETE: 'api/admin/v1/services/{id}',

    CONFIG_SETTINGS_POST: 'api/admin/v1/settings',
};

export {
    routes
}
