<?php

return [
    'active'       => [
        'required' => 'Il campo active è obbligatorio.',
        'boolean'  => 'Il campo active deve essere un booleano.',
    ],
    'serviceLangs' => [
        'required' => 'Le traduzioni sono obbligatorie.',
        '*'        => [
            'lang'        => [
                'required' => 'La lingua è obbligatorie.',
                'string'   => 'La lingua deve essere una stringa.',
            ],
            'name'        => [
                'required' => 'Il campo name è obbligatorio.',
                'string'   => 'Il campo name deve essere una stringa.',
            ],
            'description' => [
                'string'   => 'Il campo description deve essere una stringa.',
            ]
        ],
    ]
];
