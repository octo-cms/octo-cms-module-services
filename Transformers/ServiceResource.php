<?php

namespace OctoCmsModule\Services\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\PictureResource;
use OctoCmsModule\Core\Transformers\TagResource;

/**
 * Class ServiceResource
 *
 * @package OctoCmsModule\Services\Transformers
 */
class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'active'       => $this->active,
            'icon'         => $this->icon,
            'serviceLangs' => ServiceLangResource::collection($this->whenLoaded('serviceLangs')),
            'pictures'     => PictureResource::collection($this->whenLoaded('pictures')),
            'tags'         => TagResource::collection($this->whenLoaded('tagged')),
        ];
    }
}
