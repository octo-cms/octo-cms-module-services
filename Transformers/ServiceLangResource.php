<?php

namespace OctoCmsModule\Services\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceLangResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lang'        => $this->lang,
            'name'        => $this->name,
            'description' => $this->description,
            'service'     => new ServiceResource($this->whenLoaded('service')),
        ];
    }
}
