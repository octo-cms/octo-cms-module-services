const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

require('laravel-mix-alias');
mix.alias({
    '@octoCmsComponents': '../Admin/Resources/octo-cms-components',
    '@adminNodeModules': '../Admin/node_modules',
});

const assetPath = '../../public/assets-' + process.env.MIX_ADMIN_PREFIX;

const assetVuePath = assetPath + '/vue/services/'

mix.js(__dirname + '/Resources/vue/pages/services.js', assetVuePath + 'services.min.js');
mix.js(__dirname + '/Resources/vue/pages/settings.js', assetVuePath + 'settings.min.js');

if (mix.inProduction()) {
    mix.version();
}
