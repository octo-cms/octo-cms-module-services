<?php

namespace OctoCmsModule\Services\Console;

use Illuminate\Console\Command;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;

/**
 * Class InstallServicesCommand
 *
 * @package OctoCmsModule\Services\Console
 */
class InstallServicesCommand extends Command
{
    protected const MODULE_NAME = 'Services';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'install:services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';


    /**
     * @var SettingServiceInterface
     */
    private $settingService;


    /**
     * InstallServicesCommand constructor.
     * @param SettingServiceInterface $settingService
     */
    public function __construct(SettingServiceInterface $settingService)
    {
        parent::__construct();
        $this->settingService = $settingService;
    }


    public function handle()
    {

        $this->info('Running Install Services Command ...');

        $this->info('Creating Settings ...');

        $this->createSettings();
    }


    private function createSettings()
    {

        $data = [
            'page_publish' => true,
            'pictures'     => [
                'main' => [
                    'image_width'  => 500,
                    'image_height' => 500,
                ],
                'icon' => [
                    'image_width'  => 64,
                    'image_height' => 64,
                ],
            ],
            'base_url'     => [],
        ];

        $languages = $this->settingService->getSettingByName(SettingNameConst::LANGUAGES);
        foreach ($languages as $key => $lang) {
            $data["base_url"][] = [
                "value" => "services",
                "lang"  => $lang
            ];
        }

        Setting::updateOrCreate(
            ['name' => 'module_' . strtolower(config('services.name'))],
            ['value' => $data]
        );
    }
}
