<?php

namespace OctoCmsModule\Services\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use OctoCmsModule\Sitebuilder\Traits\SlugTrait;
use OctoCmsModule\Services\Factories\ServiceLangFactory;

/**
 * OctoCmsModule\Services\Entities\ServiceLang
 *
 * @property int                             $id
 * @property int                             $service_id
 * @property string                          $lang
 * @property string                          $name
 * @property string|null                     $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Service                    $service
 * @method static Builder|ServiceLang newModelQuery()
 * @method static Builder|ServiceLang newQuery()
 * @method static Builder|ServiceLang query()
 * @method static Builder|ServiceLang whereCreatedAt($value)
 * @method static Builder|ServiceLang whereDescription($value)
 * @method static Builder|ServiceLang whereId($value)
 * @method static Builder|ServiceLang whereLang($value)
 * @method static Builder|ServiceLang whereName($value)
 * @method static Builder|ServiceLang whereServiceId($value)
 * @method static Builder|ServiceLang whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $slug
 * @method static Builder|ServiceLang whereSlug($value)
 */
class ServiceLang extends Model
{
    use SlugTrait, HasFactory;

    /** @var array $fillable */
    protected $fillable = [
        'service_id',
        'lang',
        'name',
        'description'
    ];

    /**
     * @return string
     */
    protected static function newFactory()
    {
        return ServiceLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    /**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = $this->retrieveSlug($value);
    }
}
