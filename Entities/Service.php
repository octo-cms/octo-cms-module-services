<?php

namespace OctoCmsModule\Services\Entities;

use Conner\Tagging\Model\Tagged;
use Conner\Tagging\Taggable;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Sitebuilder\Traits\PageableTrait;
use OctoCmsModule\Core\Traits\PicturableTrait;
use OctoCmsModule\Services\Factories\ServiceFactory;

/**
 * OctoCmsModule\Services\Entities\Service
 *
 * @property int                                                     $id
 * @property bool                                                    $active
 * @property string|null                                             $icon
 * @property Carbon|null                                             $created_at
 * @property Carbon|null                             $updated_at
 * @property-read Page|null                 $page
 * @property-read Collection|ServiceLang[] $serviceLangs
 * @property-read int|null             $service_langs_count
 * @method static Builder|Service newModelQuery()
 * @method static Builder|Service newQuery()
 * @method static Builder|Service query()
 * @method static Builder|Service whereActive($value)
 * @method static Builder|Service whereCreatedAt($value)
 * @method static Builder|Service whereIcon($value)
 * @method static Builder|Service whereId($value)
 * @method static Builder|Service whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|Picture[] $pictures
 * @property-read int|null             $pictures_count
 * @property array                     $tag_names
 * @property-read Collection|\Tagged[] $tags
 * @property-read Collection|Tagged[]  $tagged
 * @property-read int|null             $tagged_count
 * @method static Builder|Service withAllTags($tagNames)
 * @method static Builder|Service withAnyTag($tagNames)
 * @method static Builder|Service withoutTags($tagNames)
 */
class Service extends Model
{
    use PageableTrait, PicturableTrait, Taggable, HasFactory;

    public const GCS_PATH = 'services';

    /** @var array $fillable */
    protected $fillable = [
        'active',
        'icon'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    /**
     * @return string
     */
    protected static function newFactory()
    {
        return ServiceFactory::new();
    }

    /**
     * @return HasMany
     */
    public function serviceLangs()
    {
        return $this->hasMany(ServiceLang::class);
    }
}
