<?php

namespace OctoCmsModule\Services\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class SaveServiceRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Services\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SaveServiceRequest extends FormRequest
{
    use SavePictureRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return Arr::collapse([
            [
                'active'                     => 'required|boolean',
                'icon'                       => 'present',
                'serviceLangs'               => 'required|array',
                'serviceLangs.*.lang'        => 'required|string',
                'serviceLangs.*.name'        => 'required|string',
                'serviceLangs.*.description' => 'present',
                'tags'                       => 'sometimes|array',
            ],
            $this->getPictureRules(),
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'active.required'                     => __('services::validation.active.required'),
            'active.boolean'                      => __('services::validation.active.boolean'),
            'serviceLangs.required'               => __('services::validation.serviceLangs.required'),
            'serviceLangs.*.lang.required'        => __('services::validation.serviceLangs.*.lang.required'),
            'serviceLangs.*.lang.string'          => __('services::validation.serviceLangs.*.lang.string'),
            'serviceLangs.*.name.required'        => __('services::validation.serviceLangs.*.name.required'),
            'serviceLangs.*.name.string'          => __('services::validation.serviceLangs.*.name.string'),
            'serviceLangs.*.description.string'   => __('services::validation.serviceLangs.*.description.string'),
        ];
    }
}
