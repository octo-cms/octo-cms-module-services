<?php

namespace OctoCmsModule\Services\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Core\DTO\EntityIdsDTO;
use OctoCmsModule\Core\Http\Requests\EntityIdsRequest;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Interfaces\EntityIdsServiceInterface;

/**
 * Class BlockEntityController
 *
 * @package OctoCmsModule\Services\Http\Controllers\V1
 */
class BlockEntityController extends Controller
{
    /**
     * @param EntityIdsRequest $request
     * @param EntityIdsServiceInterface $entityIdsService
     *
     * @return JsonResponse
     */
    public function blockEntityIds(
        EntityIdsRequest $request,
        EntityIdsServiceInterface $entityIdsService
    ) {
        $fields = $request->validated();

        /** @var EntityIdsDTO $entityIdsDTO */
        $entityIdsDTO = $entityIdsService->getEntityIdsDTO(
            Service::with('serviceLangs')->whereNotIn('id', Arr::get($fields, 'excludedIds', [])),
            [
                'orderBy'     => 'id',
                'currentPage' => Arr::get($fields, 'currentPage', 1),
                'rowsInPage'  => Arr::get($fields, 'rowsInPage', 12)
            ]
        );

        $entityIdsDTO->collection = $entityIdsService->parseMultiLangCollection(
            $entityIdsDTO->builder->get(),
            'service_langs',
            'id',
            'name',
            null
        );

        return response()->json($entityIdsDTO, Response::HTTP_OK);
    }
}
