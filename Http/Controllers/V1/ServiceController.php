<?php

namespace OctoCmsModule\Services\Http\Controllers\V1;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Http\Requests\SaveServiceRequest;
use OctoCmsModule\Services\Interfaces\PictureServiceInterface;
use OctoCmsModule\Services\Interfaces\ServiceServiceInterface;
use OctoCmsModule\Services\Transformers\ServiceResource;
use Throwable;

/**
 * Class ServiceController
 *
 * @package OctoCmsModule\Services\Http\Controllers\V1
 */
class ServiceController extends Controller
{
    protected $serviceService;
    protected $pictureService;

    /**
     * ServiceController constructor.
     * @param ServiceServiceInterface $serviceService
     * @param PictureServiceInterface   $pictureService
     */
    public function __construct(
        ServiceServiceInterface $serviceService,
        PictureServiceInterface $pictureService
    ) {
        $this->serviceService = $serviceService;
        $this->pictureService = $pictureService;
    }

    /**
     * @param SaveServiceRequest $request
     * @return JsonResponse|object
     * @throws Throwable
     */
    public function store(SaveServiceRequest $request)
    {
        $fields = $request->validated();

        /** @var Service */
        $service = $this->serviceService
            ->saveService(
                new Service(),
                $fields
            );

        $this->serviceService->storeServicePage($service);

        $this->pictureService->savePicturesEntity($service, Arr::get($fields, 'pictures', []));

        $service->load('serviceLangs');

        return (new ServiceResource($service))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|object
     */
    public function show($id)
    {
        /** @var Service $service */
        $service = Service::findOrFail($id);

        $service->load('serviceLangs');

        return new ServiceResource($service);
    }

    /**
     * @param SaveServiceRequest $request
     * @param                    $id
     * @return JsonResponse|object
     * @throws Throwable
     */
    public function update(SaveServiceRequest $request, $id)
    {
        $fields = $request->validated();

        /** @var Service  */
        $service = $this->serviceService
            ->saveService(
                Service::findOrFail($id),
                $fields
            );

        $this->pictureService->savePicturesEntity($service, Arr::get($fields, 'pictures', []));

        $service->load('serviceLangs');

        return (new ServiceResource($service))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|object
     * @throws Exception
     */
    public function delete($id)
    {
        /** @var Service $service */
        $service = Service::findOrFail($id);
        $service->delete();
        $service->page->delete();

        return response()->json()->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    /**
     * @param DatatableRequest          $request
     * @param DatatableServiceInterface $datatableService
     *
     * @return JsonResponse
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        /** @var Builder $services */
        $services = Service::query();

        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $services);

        $collection = $datatableDTO->builder->get();

        $collection->load('serviceLangs')
            ->load('tagged')
            ->load('pictures', 'pictures.pictureLangs');

        $datatableDTO->collection = ServiceResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }
}
