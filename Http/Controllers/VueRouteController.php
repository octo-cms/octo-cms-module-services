<?php

namespace OctoCmsModule\Services\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class VueRouteController
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Services\Http\Controllers
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class VueRouteController extends Controller
{
    /**
     * Name services
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function services()
    {
        return view(
            'admin::vue-full-page',
            ['script' => 'services/services']
        );
    }

    /**
     * Name settings
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings()
    {
        return view(
            'admin::vue-full-page',
            ['script' => 'services/settings']
        );
    }
}
