<?php

Route::group(['prefix' => env('MIX_ADMIN_PREFIX'), 'middleware' => ['auth-admin']], function () {
    Route::group(['prefix' => 'services'], function () {
        Route::get(
            'settings',
            getRouteAction("Services", "VueRouteController", 'settings')
        )
            ->name('admin.vue-route.services.settings');

        Route::get(
            '',
            getRouteAction("Services", "VueRouteController", 'services')
        )
            ->name('admin.vue-route.services');
    });
});
