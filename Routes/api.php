<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admin/v1'], function () {

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::group(['prefix' => 'services'], function () {
            Route::post(
                'block-entity-ids',
                getRouteAction("Services", "V1\BlockEntityController", 'blockEntityIds')
            )
                ->name('admin.services.block.entity.ids');

            Route::post('', getRouteAction("Services", "V1\ServiceController", 'store'))
                ->name('admin.services.store');

            Route::group(['prefix' => '{id}'], function () {
                Route::get('', getRouteAction("Services", "V1\ServiceController", 'show'))
                    ->name('admin.services.show');
                Route::delete('', getRouteAction("Services", "V1\ServiceController", 'delete'))
                    ->name('admin.services.delete');
                Route::put('', getRouteAction("Services", "V1\ServiceController", 'update'))
                    ->name('admin.services.update');
            });
        });

        Route::group(['prefix' => 'datatables'], function () {
            Route::post('services', getRouteAction("Services", "V1\ServiceController", 'datatableIndex'))
                ->name('admin.datatables.services');
        });
    });
});
