<?php

return [
    'name'  => 'services',
    'admin' => [
        'sidebar' => [
            [
                'order'    => 5,
                'label'    => 'services',
                'childs'   => [
                    [
                        'label' => 'list',
                        'route' => 'admin.vue-route.services',
                    ],
                ],
                'settings' => [
                    [
                        'label' => 'settings',
                        'route' => 'admin.vue-route.services.settings',
                    ]
                ],
            ],
        ],
    ],
];
