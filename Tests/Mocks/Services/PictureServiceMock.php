<?php

namespace OctoCmsModule\Services\Tests\Mocks\Services;

use OctoCmsModule\Core\Tests\Mocks\PictureServiceMock as PictureServiceMockCore;
use OctoCmsModule\Services\Interfaces\PictureServiceInterface;

/**
 * Class PictureServiceMock
 *
 * @package OctoCmsModule\Services\Tests\Mocks\Services
 */
class PictureServiceMock extends PictureServiceMockCore implements PictureServiceInterface
{

}
