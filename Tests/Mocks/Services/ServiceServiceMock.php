<?php

namespace OctoCmsModule\Services\Tests\Mocks\Services;

use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Interfaces\ServiceServiceInterface;

class ServiceServiceMock implements ServiceServiceInterface
{
    /**
     * @param Service $service
     * @param array   $fields
     *
     * @return Service
     */
    public function saveService(Service $service, array $fields): Service
    {
        return Service::factory()->create();
    }

    /**
     * @param Service $service
     *
     * @return Page
     */
    public function storeServicePage(Service $service): Page
    {
        return Page::factory()->create([
            'type'          => Page::TYPE_SERVICE,
            'pageable_type' => Page::PAGEABLE_TYPE_SERVICE,
            'pageable_id'   => $service->id,
        ])->first();
    }

}
