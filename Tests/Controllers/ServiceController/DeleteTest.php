<?php

namespace OctoCmsModule\Services\Tests\Controllers\ServiceController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Services\Entities\Service;

/**
 * Class DeleteTest
 *
 * @package OctoCmsModule\Services\Tests\Controllers\ServiceController
 */
class DeleteTest extends TestCase
{


    public function test_delete()
    {
        /** @var Service $service */
        $service = Service::factory()->create();

        Page::factory()->create([
            'pageable_type' => 'Service',
            'pageable_id'   => $service->id,
        ])->first();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'DELETE',
            route('admin.services.delete', ['id' => $service->id])
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('services', [
            'id'     => $service->id,
        ]);

        $this->assertEmpty($service->page()->get());
    }
}
