<?php

namespace OctoCmsModule\Services\Tests\Controllers\ServiceController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Services\Tests\Controllers\ServiceController
 */
class StoreTest extends TestCase
{


    public function dataProvider()
    {
        $providers = [];

        $data = [
            'active'       => true,
            'icon'         => 'icon',
            'serviceLangs' => [
                [
                    'lang'        => 'it',
                    'name'        => 'nome',
                    'description' => 'descrizione',
                ],
            ],
            'tags'         => ['tag 1', 'tag 2'],
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['active']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['serviceLangs']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['serviceLangs'][0]['lang']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['serviceLangs'][0]['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['serviceLangs'][0]['description']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['tags']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_store(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('admin.services.store'),
            $fields
        );

        $response->assertStatus($status);
    }
}
