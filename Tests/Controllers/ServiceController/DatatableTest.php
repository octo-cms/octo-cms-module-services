<?php

namespace OctoCmsModule\Services\Tests\Controllers\ServiceController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Services\Entities\Service;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Services\Tests\Controllers\V1\ServiceController
 */
class DatatableTest extends TestCase
{


    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        Service::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.services');
    }
}
