<?php

namespace OctoCmsModule\Services\Tests\Controllers\ServiceController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;


/**
 * Class ShowTest
 *
 * @package OctoCmsModule\Services\Tests\Controllers\ServiceController
 */
class ShowTest extends TestCase
{


    public function test_show()
    {
        /** @var Service $service */
        $service = Service::factory()->has(ServiceLang::factory()->count(2))->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('admin.services.show', ['id' => $service->id])
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
                'id'     => $service->id,
                'active' => $service->active
        ]);
    }
}
