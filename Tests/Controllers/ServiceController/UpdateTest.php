<?php

namespace OctoCmsModule\Services\Tests\Controllers\ServiceController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Services\Entities\Service;


/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Services\Tests\Controllers\ServiceController
 */
class UpdateTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            'active'       => true,
            'icon'         => 'icon',
            'serviceLangs' => [
                [
                    'lang'        => 'it',
                    'name'        => 'nome',
                    'description' => 'descrizione',
                ],
            ],
            'tags'         => ['tag 1', 'tag 2'],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['active']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];


        $fields = $data;
        unset($fields['serviceLangs']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['serviceLangs'][0]['lang']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['serviceLangs'][0]['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['serviceLangs'][0]['description']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['tags']);
        $providers[] = [$fields, Response::HTTP_OK];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_update(array $fields, int $status)
    {
        /** @var Service $service */
        $service = Service::factory()->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'PUT',
            route('admin.services.update', ['id' => $service->id]),
            $fields
        );

        $response->assertStatus($status);
    }
}
