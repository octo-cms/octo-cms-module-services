<?php

namespace OctoCmsModule\Services\Tests\Controllers\VueRouteController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class VueRouteTest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Services\Tests\Controllers\VueRouteController
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class VueRouteTest extends TestCase
{


    /**
     * Name dataProvider
     *
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = ['admin.vue-route.services'];
        $providers[] = ['admin.vue-route.services.settings'];

        return $providers;
    }

    /**
     * Name test_routes
     *
     * @dataProvider dataProvider
     * @return void
     *
     */
    public function test_routes($route)
    {
        Sanctum::actingAs(self::createAdminUser());
        $this->withoutMix();
        $response = $this->json( 'GET', route($route) );

        $response->assertStatus(Response::HTTP_OK);

    }
}
