<?php

namespace OctoCmsModule\Services\Tests\Controllers\BlockEntityController;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Services\Entities\Service;

/**
 *
 * Class BlockEntityIdsTest
 * @package OctoCmsModule\Testimonials\Tests\Controllers\V1\BlockEntityController
 */
class BlockEntityIdsTest extends TestCase
{



    public function test_blockEntityIds()
    {
        Sanctum::actingAs(self::createAdminUser());

        Service::factory()->count(15)->create();

        $response = $this->json(
            'POST',
            route('admin.services.block.entity.ids'),
            [
                'currentPage' => 1,
                'rowsInPage'  => 10,
                'excludedIds' => []
            ]
        );

        $content = json_decode($response->getContent(), true);

        $response->assertStatus(Response::HTTP_OK);


        $this->assertEquals(15, Arr::get($content, 'total', 0));

        $this->assertEquals(1, Arr::get($content, 'currentPage', 0));

        $this->assertEquals(10, Arr::get($content, 'rowsInPage', 0));

        $this->assertNotEmpty(Arr::get($content, 'collection', []));

    }
}
