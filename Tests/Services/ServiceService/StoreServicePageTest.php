<?php

namespace OctoCmsModule\Services\Tests\Services\ServiceService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;
use OctoCmsModule\Services\Services\ServiceService;

/**
 * Class StoreServicePageTest
 *
 * @package OctoCmsModule\Services\Tests\Services\ServiceService
 */
class StoreServicePageTest extends TestCase
{


    public function test_storePage()
    {
        /** @var Service $service */
        $service = Service::factory()->create();

        /** @var ServiceLang $itServiceLang */
        $itServiceLang = ServiceLang::factory()->create([
            'service_id' => $service->id,
            'lang'       => 'it',
            'name'       => 'nome del servizio',
        ]);

        /** @var ServiceLang $enServiceLang */
        $enServiceLang = ServiceLang::factory()->create([
            'service_id' => $service->id,
            'lang'       => 'en',
            'name'       => 'service name',
        ]);

        /** @var ServiceService $serviceService */
        $serviceService = new ServiceService();

        /** @var Page $servicePage */
        $servicePage = $serviceService->storeServicePage($service);

        $this->assertDatabaseHas('pages', [
            'id'   => 1,
            'name' => 'nome del servizio',
        ]);

        $this->assertDatabaseHas('page_langs', [
            'url'              => 'services/nome-del-servizio',
            'page_id'          => $servicePage->id,
            'lang'             => $itServiceLang->lang,
            'meta_title'       => $itServiceLang->name,
            'meta_description' => $itServiceLang->description,
        ]);

        $this->assertDatabaseHas('page_langs', [
            'url'              => 'services/service-name',
            'page_id'          => $servicePage->id,
            'lang'             => $enServiceLang->lang,
            'meta_title'       => $enServiceLang->name,
            'meta_description' => $enServiceLang->description,
        ]);
    }
}
