<?php

namespace OctoCmsModule\Services\Tests\Services\ServiceService;

use Illuminate\Support\Str;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;
use OctoCmsModule\Services\Services\ServiceService;

/**
 * Class SaveServiceTest
 *
 * @package OctoCmsModule\Services\Tests\Services\ServiceService
 */
class SaveServiceTest extends TestCase
{



    public function test_storeService()
    {
        /** @var array $tags */
        $tags = ['tag 1', 'tag 2'];

        /** @var array $fields */
        $fields = [
            'active'       => true,
            'icon'         => 'icon',
            'name'         => 'nome',
            'serviceLangs' => [
                [
                    'lang'        => 'it',
                    'name'        => 'nome',
                    'description' => 'descrizione',
                ],
            ],
            'tags'         => $tags,
        ];

        /** @var ServiceService $serviceService */
        $serviceService = new ServiceService();

        $serviceService->saveService(new Service(), $fields);

        $this->assertDatabaseHas('services', [
            'id'     => 1,
            'active' => $fields['active'],
            'icon'   => $fields['icon'],
        ]);

        foreach ($fields['serviceLangs'] as $serviceLang) {
            $this->assertDatabaseHas('service_langs',
                [
                    'service_id'  => 1,
                    'lang'        => $serviceLang['lang'],
                    'name'        => $serviceLang['name'],
                    'slug'        => Str::slug($serviceLang['name']),
                    'description' => $serviceLang['description'],
                ]
            );
        }

        foreach ($tags as $tag) {
            $this->assertDatabaseHas('tagging_tagged', [
                'taggable_id'   => 1,
                'taggable_type' => 'Service',
                'tag_name'      => ucwords($tag),
            ]);
        }
    }

    public function test_updateService()
    {
        /** @var array $updatedTags */
        $updatedTags = ['tag 1 updated', 'tag 2 updated'];

        /** @var array $fields */
        $fields = [
            'active'       => false,
            'icon'         => 'new icon',
            'serviceLangs' => [
                [
                    'lang'        => 'it',
                    'name'        => 'programmazione',
                    'description' => 'Io programmo',
                ],
                [
                    'lang'        => 'en',
                    'name'        => 'programming',
                    'description' => 'I program',
                ],
            ],
            'tags'         => $updatedTags,
        ];

        /** @var Service $service */
        $service = Service::factory()->create([
            'active' => true,
            'icon'   => 'icon',
        ]);

        $service->tag(['tag 1', 'tag 2']);

        ServiceLang::factory()->create([
            'service_id'  => $service->id,
            'lang'        => 'it',
            'name'        => 'pulizie',
            'description' => 'Io pulisco',
        ]);

        ServiceLang::factory()->create([
            'service_id'  => $service->id,
            'lang'        => 'en',
            'name'        => 'cleaning',
            'description' => 'I clean',
        ]);

        /** @var serviceService $serviceService */
        $serviceService = new serviceService();

        $serviceService->saveService($service, $fields);

        $this->assertDatabaseHas('services', [
            'id'     => $service->id,
            'active' => $fields['active'],
            'icon'   => $fields['icon'],
        ]);

        foreach ($fields['serviceLangs'] as $serviceLang) {
            $this->assertDatabaseHas('service_langs',
                [
                    'service_id'  => $service->id,
                    'lang'        => $serviceLang['lang'],
                    'name'        => $serviceLang['name'],
                    'slug'        => Str::slug($serviceLang['name']),
                    'description' => $serviceLang['description'],
                ]
            );
        }

        foreach ($updatedTags as $tag) {
            $this->assertDatabaseHas('tagging_tagged', [
                'taggable_id'   => $service->id,
                'taggable_type' => 'Service',
                'tag_name'      => ucwords($tag),
            ]);
        }
    }
}
