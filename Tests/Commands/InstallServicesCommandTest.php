<?php

namespace OctoCmsModule\Services\Tests\Commands;

use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Services\Console\InstallServicesCommand;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class InstallServicesCommandTest
 *
 * @package OctoCmsModule\Services\Tests\Commands
 */
class InstallServicesCommandTest extends TestCase
{


    /**
     * @var string
     */
    private $moduleName = "";

    /**
     * @var string[]
     */
    private $languages = ['it', 'en'];


    public function setUp(): void
    {
        parent::setUp();

        $this->moduleName = 'module_' . strtolower(config('services.name'));

        Setting::create([
            'name'  => 'languages',
            'value' => $this->languages,
        ]);

    }

    public function test_installServicesCommand()
    {

        $this->artisan('install:services')
            ->expectsOutput('Running Install Services Command ...')
            ->expectsOutput('Creating Settings ...')
            ->assertExitCode(0);

        $this->assertDatabaseHas('settings', [
            'name' => $this->moduleName,
        ]);
    }

    public function test_installServicesCommandWithDataPresentInSetting()
    {

        Setting::create([
            'name'  => $this->moduleName,
            'value' => $this->languages,
        ]);

        $this->artisan('install:services')
            ->expectsOutput('Creating Settings ...')
            ->assertExitCode(0);

        $this->assertDatabaseHas('settings', [
            'name' => $this->moduleName,
        ]);
    }

    /**
     * @throws ReflectionException
     */
    public function test_createSettings()
    {
        /** @var InstallServicesCommand $installServicesCommand */
        $installServicesCommand = new InstallServicesCommand(new SettingServiceMock());

        $this->invokeMethod($installServicesCommand, 'createSettings');

        $this->assertDatabaseHas('settings', [
            'name'  => 'module_services',
            'value' => serialize([
                'page_publish' => true,
                'pictures'     => [
                    'main' => [
                        'image_width'  => 500,
                        'image_height' => 500,
                    ],
                    'icon' => [
                        'image_width'  => 64,
                        'image_height' => 64,
                    ],
                ],
                'base_url'     => [
                    [
                        'value' => 'services',
                        'lang'  => 'it',
                    ],
                    [
                        'value' => 'services',
                        'lang'  => 'en',
                    ],
                ],
            ]),
        ]);
    }
}
