<?php

namespace OctoCmsModule\Services\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ServiceTest
 *
 * @package OctoCmsModule\Services\Tests\Entities
 */
class ServiceTest extends TestCase
{


    public function test_ServiceHasManyServiceLangs()
    {
        /** @var Service $service */
        $service = Service::factory()
            ->has(ServiceLang::factory()->count(2))
            ->create();

        $service->load('serviceLangs');

        $this->assertInstanceOf(Collection::class, $service->serviceLangs);
        $this->assertInstanceOf(ServiceLang::class, $service->serviceLangs->first());
    }

    public function test_ServiceMorphOnePage()
    {
        /** @var Service $service */
        $service = Service::factory()->create();

        /** @var Page $page */
        $page = Page::factory()->create()->first();

        $page->pageable()->associate($service)->save();

        $page->load('pageable');

        $this->assertInstanceOf(Service::class, $page->pageable);
    }

    public function test_PageHasOneService()
    {
        /** @var Page $page */
        $page = Page::factory()->create()->first();

        /** @var Service $service */
        $service = Service::factory()->create();

        $page->pageable()->associate($service)->save();

        $page->load('pageable');

        $this->assertEquals(
            'Service',
            $page->pageable_type
        );

        $this->assertInstanceOf(Service::class, $page->pageable);
    }
}
