<?php

namespace OctoCmsModule\Services\Tests\Entities;

use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ServiceLangTest
 *
 * @package OctoCmsModule\Services\Tests\Entities
 */
class ServiceLangTest extends TestCase
{


    public function test_ServiceLangBelongsToService()
    {
        /** @var ServiceLang $serviceLang */
        $serviceLang = ServiceLang::factory()->create();

        $serviceLang->load('service');

        $this->assertInstanceOf(Service::class, $serviceLang->service);
    }
}
