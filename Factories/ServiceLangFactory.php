<?php

namespace OctoCmsModule\Services\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;

/**
 * Class ServiceFactory
 *
 * @package OctoCmsModule\Services\Factories
 */
class ServiceLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServiceLang::class;




    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'service_id'  => Service::factory(),
            'lang'        => $this->faker->randomElement(['it', 'en', 'fr']),
            'name'        => $this->faker->name,
            'description' => $this->faker->text(100),
        ];
    }
}
