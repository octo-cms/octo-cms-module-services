<?php

namespace OctoCmsModule\Services\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Services\Entities\Service;

/**
 * Class ServiceFactory
 *
 * @package OctoCmsModule\Services\Factories
 */
class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'active' => true,
        ];
    }
}
