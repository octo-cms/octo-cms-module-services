<?php

namespace OctoCmsModule\Services\Services;

use Illuminate\Support\Arr;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Services\Entities\ServiceLang;
use OctoCmsModule\Services\Interfaces\ServiceServiceInterface;
use OctoCmsModule\Core\Utils\LanguageUtils;

/**
 * Class ServiceService
 *
 * @package OctoCmsModule\Services\Services
 */
class ServiceService implements ServiceServiceInterface
{
    /**
     * @param Service $service
     * @param array   $fields
     *
     * @return Service
     */
    public function saveService(Service $service, array $fields): Service
    {
        $service->fill($fields);
        $service->save();

        $service->retag(Arr::get($fields, 'tags', []));

        $this->saveServiceLangs($service, Arr::get($fields, 'serviceLangs', []));

        return $service;
    }

    /**
     * @param Service $service
     * @param array   $serviceLangs
     */
    protected function saveServiceLangs(Service $service, array $serviceLangs)
    {
        foreach ($serviceLangs as $serviceLang) {
            ServiceLang::updateOrCreate(
                [
                    'service_id' => $service->id,
                    'lang'       => Arr::get($serviceLang, 'lang', ''),
                ],
                [
                    'name'        => Arr::get($serviceLang, 'name', ''),
                    'description' => Arr::get($serviceLang, 'description', ''),
                ]
            );
        }
    }

    /**
     * @param Service $service
     *
     * @return Page
     */
    public function storeServicePage(Service $service): Page
    {
        $pageName = LanguageUtils::getLangValue($service->serviceLangs, 'name');

        /** @var Page $page */
        $page = new Page([
            'name' => !empty($pageName) ? $pageName : (Page::TYPE_SERVICE . '-' . $service->id),
            'type' => Page::TYPE_SERVICE,
        ]);

        $page->pageable()->associate($service)->save();

        foreach ($service->serviceLangs as $serviceLang) {
            $pageLang = new PageLang([
                'url'              => PageLang::PREFIX_SERVICES . $serviceLang->slug,
                'lang'             => $serviceLang->lang,
                'meta_title'       => $serviceLang->name,
                'meta_description' => $serviceLang->description,
            ]);

            $pageLang->page()->associate($page)->save();
        }

        return $page;
    }
}
