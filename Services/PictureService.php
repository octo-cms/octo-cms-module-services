<?php

namespace OctoCmsModule\Services\Services;

use OctoCmsModule\Core\Services\PictureService as PictureServiceCore;
use OctoCmsModule\Services\Interfaces\PictureServiceInterface;

/**
 * Class PictureService
 *
 * @package OctoCmsModule\Services\Services
 */
class PictureService extends PictureServiceCore implements PictureServiceInterface
{

}
